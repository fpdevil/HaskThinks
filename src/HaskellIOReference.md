# Haskell IO Functions descriptions
========================

References [LYAH](http://learnyouahaskell.com/input-and-output)

Haskell IO
--------

- getContents
```haskell
getContents :: IO String 	-- Defined in ‘System.IO’
```

- hGetContents
```haskell
hGetContents :: Handle -> IO String
```

> getContents will automatically read from the standard input or terminal.
> hGetContents takes a file handle which tells it which file to read from.
> The handle is something which tells what fle it is. Its just a
> pointer to the file. After the operations are completed, the handler
> has to be properly closed. We use hClose, which takes a handle and
> returns an I/O action that closes the file. It has to be closes
> manually after opening it with openFile!

getContents and hGetContents work very similar expect to the above differences. Both of them are Lazy and won't attempt to read the file at once and store it in memory, but it will read it as needed.

- An example using the hGetContents
```haskell
    import System.IO

    main = do
        handle <- openFile "examplefile.txt" ReadMode
        contents <- hGetContents handle
        putStr contents
        hClose handle
```

- An example using withFile
>withFile opens the file and then passes the handle to the function
>passed to it. It gets an I/O action back from that function and then
>makes an I/O action that's just like it, only it closes the file afterwards.
> Function withFIle has the below syntax
```haskell
withFile :: FilePath -> IOMode -> (Handle -> IO r) -> IO r
```

```haskell
    import System.IO
    -- a lambda function is used
    main = do
        withFile "examplefile.txt" ReadMode (\handle -> do
            contents <- hGetContents handle
            putStr contents)
```

> **Similar IO Functions:**

> hGetLine, hPutStr, hPutStrLn, hGetChar, etc.

They work just like their counterparts without the h, only they take a
handle as a parameter and operate on that specific file instead of
operating on standard input or standard output. Example: putStrLn
is a function that takes a string and returns an I/O action that
will print out that string to the terminal and a newline after
it. hPutStrLn takes a handle and a string and returns an I/O
action that will write that string to the file associated with the
handle and then put a newline after it. In the same vein, hGetLine
takes a handle and returns an I/O action that reads a line from
its file.


> **Other IO Functions for file reading and writing:**

> readFile, writeFile, hPutStrLn, appendFile.

- readFile

```haskell
    readFile :: FilePath -> IO String
```

- writeFile

```haskell
    writeFile :: FilePath -> String -> IO ()
```

- appendFile

```haskell
    appendFile :: FilePath -> String -> IO ()
```

### Examples for the above 3 functions
``` haskell
import System.IO

main = do
    contents <- readFile "examplefile.txt"
    putStr contents

main = do
    contents <- readFile "examplefile.txt"
    writeFile "outputFile.txt" (map toUpper contents)

main = do
    item <- getLine
    appendfile "file.txt"  (item ++ "\n")
```


You can control how exactly buffering is done by using the
`hSetBuffering` function. It takes a handle and a BufferMode and
returns an I/O action that sets the buffering. BufferMode is a simple
enumeration data type and the possible values it can hold are:
`NoBuffering, LineBuffering or BlockBuffering (Maybe Int)`.
The `Maybe Int` is for how big the chunk should be, in bytes.
If it's Nothing, then the operating system determines the chunk size.
`NoBuffering` means that it will be read one character at a time.
`NoBuffering` usually sucks as a buffering mode because it has to
access the disk so much.
Reading files in bigger chunks can help if we want to minimize disk
access or when our file is actually a slow network resource.

> Here's an example, in which data is read line by
> line but reads the whole file in chunks of 2048 bytes.


```haskell
main = do
    withFile "exampleFile.txt" ReadMode (\handle -> do
            hSetBuffering handle $ BlockBuffering (Just 2048)
            contents <- hGetContents handle
            putStr contents)
```

> - Function definitions

```haskell
hFlush :: Handle -> IO ()

hSetBuffering :: Handle -> BufferMode -> IO ()

openTempFile :: FilePath -> String -> IO (FilePath, Handle)

hPutStr :: Handle -> String -> IO ()
```

### Command line argumnets are through getArgs function


- The module System.Environment has the following functions
```haskell
getArgs :: IO [String]

getProgName :: IO String
```