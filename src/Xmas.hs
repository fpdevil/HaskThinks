module Xmas where

import           Control.Applicative
import           Data.Bits
import           Data.Bool
import qualified Data.List                         as L
import qualified Math.Combinatorics.Exact.Binomial as MB

-- Startup Data
-- All row data from Top to Bottom
rowsData :: [[Int]]
rowsData = [
               [7, 3, 1, 1, 7]
             , [1, 1, 2, 2, 1, 1]
             , [1, 3, 1, 3, 1, 1, 3, 1]
             , [1, 3, 1, 1, 6, 1, 3, 1]
             , [1, 3, 1, 5, 2, 1, 3, 1]
             , [1, 1, 2, 1, 1]
             , [7, 1, 1, 1, 1, 1, 7]
             , [3, 3]
             , [1, 2, 3, 1, 1, 3, 1, 1, 2]
             , [1, 1, 3, 2, 1, 1]
             , [4, 1, 4, 2, 1, 2]
             , [1, 1, 1, 1, 1, 4, 1, 3]
             , [2, 1, 1, 1, 2, 5]
             , [3, 2, 2, 6, 3, 1]
             , [1, 9, 1, 1, 2, 1]
             , [2, 1, 2, 2, 3, 1]
             , [3, 1, 1, 1, 1, 5, 1]
             , [1, 2, 2, 5]
             , [7, 1, 2, 1, 1, 1, 3]
             , [1, 1, 2, 1, 2, 2, 1]
             , [1, 3, 1, 4, 5, 1]
             , [1, 3, 1, 3, 10, 2]
             , [1, 3, 1, 1, 6, 6]
             , [1, 1, 2, 1, 1, 2]
             , [7, 2, 1, 2, 5]
             ]

-- All column data from Left to Right
colsData :: [[Int]]
colsData = [
               [7, 2, 1, 1, 7]
             , [1, 1, 2, 2, 1, 1]
             , [1, 3, 1, 3, 1, 3, 1, 3, 1]
             , [1, 3, 1, 1, 5, 1, 3, 1]
             , [1, 3, 1, 1, 4, 1, 3, 1]
             , [1, 1, 1, 2, 1, 1]
             , [7, 1, 1, 1, 1, 1, 7]
             , [1, 1, 3]
             , [2, 1, 2, 1, 8, 2, 1]
             , [2, 2, 1, 2, 1, 1, 1, 2]
             , [1, 7, 3, 2, 1]
             , [1, 2, 3, 1, 1, 1, 1, 1]
             , [4, 1, 1, 2, 6]
             , [3, 3, 1, 1, 1, 3, 1]
             , [1, 2, 5, 2, 2]
             , [2, 2, 1, 1, 1, 1, 1, 2, 1]
             , [1, 3, 3, 2, 1, 8, 1]
             , [6, 2, 1]
             , [7, 1, 4, 1, 1, 3]
             , [1, 1, 1, 1, 4]
             , [1, 3, 1, 3, 7, 1]
             , [1, 3, 1, 1, 1, 2, 1, 1, 4]
             , [1, 3, 1, 4, 3, 3]
             , [1, 1, 2, 2, 2, 6, 1]
             , [7, 1, 3, 2, 1, 1]
             ]

-- All prefilled cell data in tuple format from top left
-- fst  = row and snd = col for each tuple line
givenData :: [(Int, Int)]
givenData = [
                (3, 3), (3, 4), (3, 12), (3, 13), (3, 21)
              , (8, 6), (8, 7), (8, 10), (8, 14), (8, 15), (8, 18)
              , (16, 6), (16, 11), (16, 16), (16, 20)
              , (21, 3), (21, 4), (21, 9), (21, 10), (21, 15), (21, 20), (21, 21)
              ]

{-
States of each inner square in a row
Known Square   = Just True
Unknown Square = Nothing
Filled         = True
Empty          = False
-}

-- create a grid of Maybe Bool values for the rows
-- the values from the givenData is used to mark as
-- Just True and the rest all which are currently
-- unknown will all be Nothing for now.
ambiguosGrid :: [(Int, Int)] -> [[Maybe Bool]]
ambiguosGrid d = [[if (r-1, c-1) `elem` d then Just True else Nothing | c <- [1 .. 25]] | r <- [1 .. 25]]

-- Count the number of unknown cells
-- We can use this function to check if the puzzle is solved
countUnknowns :: [[Maybe Bool]] -> Int
countUnknowns x = sum $ fmap (length . filter (== Nothing)) x

-- for testing count the initial unknowns
initialUnknowns :: Int
initialUnknowns = countUnknowns (ambiguosGrid givenData)

-- For the above initial grid in the binary 0 1 form
showPreGridBin :: [[Maybe Bool]] -> [[Int]]
showPreGridBin = (fmap . fmap) (\x -> if x == Just True then 1 else 0)

-- For displaying the binary pre filled grid
displayPreGrid :: IO ()
displayPreGrid = mapM_ print $ showPreGridBin (ambiguosGrid givenData)

-- predicate based on a parameter
-- takes a value and a maybe value
-- if the second argument is Maybe something,
-- function f is applied over the value of Maybe
-- otherwise will return first argument as default
checkMaybe :: b -> Maybe a -> (a -> b) -> b
checkMaybe x Nothing _ = x
checkMaybe _ (Just y) f = f y

-- check 2 lists and return Just possibilities or Nothing
-- one list contains all uncertain values
-- other list contains probable values
checkBoolLists :: [Maybe Bool] -> [Bool] -> Maybe [Bool]
checkBoolLists xs ys = if length xs == length ys &&
                          and (zipWith (\a b -> checkMaybe True a (== b)) xs ys)
                          then Just ys
                          else Nothing

-- check the spaces required for each filled cells
spacesRequired :: [Int] -> Int
spacesRequired [] = 0
spacesRequired xs = sum xs + length xs - 1

-- display the Board
-- prefilled cells      = #
-- original empty cells = empty space
-- all unknowns         = ?
displayBoard :: [[Maybe Bool]] -> [[String]]
displayBoard = (fmap . fmap) showB
             where
             showB x = case x of
                         Nothing -> " "
                         Just y -> if y
                                    then "#"
                                    else "?"

-- λ> mapM_ print $ displayBoard $ ambiguosGrid givenData


{-
    solverc  = all combinations of the cells which match the given clues in a row/col
    clue     = length of each inidividual filled blocks in a row (numbers on the left)
    block    = a set of consecutive filled cells
    cell     = a single entity of a row or a column
    gap      = an unfilled or an empty cell

    get all possible movable spaces which can occur for each row based
    based on the given values of the row or column (clues).

    These spaces will be ones which have mulitple possible locations
    For the clue provided for Row [7, 3, 1, 1, 7] and length = 25,
    We have 5 blocks with 19 (7 + 3 + 1 + 1 + 7) filled cells

    The total number of possible positions = length(clues) + 1 = 5 + 1
    So there are 6 positions where we can have spaces
    with terminology of spaces = 0, filled = 1
    [0,1,1,1,1,1,1,1,0,1,1,1,0,1,0,1,0,1,1,1,1,1,1,1,0]

    For the sequence, we must have atleast 4 spaces in between for the
    given 5 filled blocks. So, out of the 6 possible positions for the
    spaces we must have atleast 4 spaces which should be fixed. Hence
    the remaining (25 - 19 - 4) = 2 spaces can be moved freely.
    The remaining 2 freely movable spaces can be arranged in any of the
    following ways

λ> [x| x <- mapM (const [0, 1, 2, 3, 4, 5]) [1..2], head x <= head (tail x) ]
[ [ 0 , 0 ]
, [ 0 , 1 ]
, [ 0 , 2 ]
, [ 0 , 3 ]
, [ 0 , 4 ]
, [ 0 , 5 ]
, [ 1 , 1 ]
, [ 1 , 2 ]
, [ 1 , 3 ]
, [ 1 , 4 ]
, [ 1 , 5 ]
, [ 2 , 2 ]
, [ 2 , 3 ]
, [ 2 , 4 ]
, [ 2 , 5 ]
, [ 3 , 3 ]
, [ 3 , 4 ]
, [ 3 , 5 ]
, [ 4 , 4 ]
, [ 4 , 5 ]
, [ 5 , 5 ]
]

    This gives a total of 21 possible combinations. The above can be
    simplified to the below mathematical formula
    Using Combinatorial Math
    (ₙ ₊ ᵣ ₋ ₁)Cᵣ where n ≻ 0
    i.e., (n + r - 1)！/ r！(n - 1)！

    For 6 possible positions, and 2 free spaces,
    number of ways the 2 free spaces can be chosen from 6 available
    is arranged as
    n = 6, r = 2
    (6 + 2 - 1)! / (2! * (6-1)!) = 21 = ⁷C₂

    For [1, 1, 2, 2, 1, 1] row, n = 6 + 1 = 7
    r = free spaces = 12
    combinations = ¹⁸C₁₂ = 10564

    The main concept is to have atleast 1 single space between each blocks.
    Its calculated as length (row) - (sum of clues + length of clues)
-}

-- possible positions for spaces in a row
possibleRowPos :: [[Int]] -> [Int]
possibleRowPos = fmap (\x -> 1 + length x)

-- fixed row positions
fixedRowPos :: [[Int]] -> [Int]
fixedRowPos = fmap (\x -> length x - 1)

-- free or movable gaps available
freeGaps :: [[Int]] -> [Int]
freeGaps rc = fmap length xs
                     where
                     xs = [[0 .. length rc - (sum clues + length clues)] | clues <- rc]

-- λ> freeGaps rowsData
-- [2,12,4,1,1,15,0,18,2,11,6,5,8,3,5,9,6,12,3,9,5,0,2,12,4]
--

-- available number of combinations of spaces in each row
rowCombinations :: [[Int]] -> [Int]
rowCombinations xs = getZipList $ (\n r -> MB.choose (n + r - 1) r)
                                <$> ZipList (possibleRowPos xs)
                                <*> ZipList (freeGaps xs)

-- λ> rowCombinations rowsData
-- [21,18564,495,9,9,15504,1,190,55,12376,924,1287,3003,84,462,5005,1716,1820,120,11440,462,1,28,18564,126]

-- Take a clue and length of a solver, generate all the possible solutions

-- Binomial Coefficient
-- (a ∨ b, a ∧ b) ∀ a ∈ [0 .. n], b ∈ [0, n]
f :: (Enum t, Num t, Bits t) => t -> [(t, t)]
f n = [(a .|. b, a .&. b) | a <- [0 .. n], b <- [0 .. n]]

-- pascals triangle for odd numbers
plotf :: (Enum t, Num t, Bits t) => t -> String
plotf n = unlines [[bool ' ' '*' $ (r, c) `elem` f n | c <- [0 .. n]] | r <- [0 .. n]]

showPlt :: Int -> IO ()
showPlt n = putStr $ plotf (n :: Int)

-- ₙCᵣ = ₙ₋₁Cᵣ₋₁ + ₙCᵣ₋₁
choose :: Integer -> Integer -> Integer
choose _ 0 = 1
choose n k
  | n < k     = 0
  | otherwise = choose (n - 1) (k - 1) + choose (n - 1) k

-- Plot the above
plot :: Integer -> String
plot n = unlines [[bool ' ' '*' $ odd $ choose r c | c <- [0 .. n]] | r <- [0 .. n]]
